import "./App.css";
import React from "react";
import styled from "styled-components";
// eslint-disable-next-line
import { Link, Route, Routes, BrowserRouter } from "react-router-dom";
import GoogleButton from "react-google-button";
import { LoginSuccess } from "./app/containers";
import { useDispatch, useSelector } from "react-redux";
import { setAuthUser, setIsAuthenticated } from "./app/appSlice";
import axios from "axios";

// eslint-disable-next-line
const AppContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 31px;
`;
const GoogleContainer = styled.div`
  box-sizing: border-box;
  margin: 100px;
  padding: 100px;
`;

const LoginContainer = styled.div`
  line-height: 1.15;
  text-size-adjust: 100%;
  font-size: 16px;
  color: rgb(48, 48, 48);
  font-family: "LATAM Sans", "Trebuchet MS", sans-serif;
  box-sizing: border-box;
  margin: 0px;
  padding: 0px;
  border: 0px;
  margin-block: 0px;
  margin-inline: 0px;
  padding-block: 0px;
  padding-inline: 0px;
  margin-top: 3rem;
  border-top: 0.5rem solid rgb(27, 0, 136);
  border-radius: 1rem;
  box-shadow: rgba(16, 0, 79, 0.12) 0px 0.125rem 0.5rem;
  background-color: rgb(255, 255, 255);
  width: 29rem;
`;

const HeaderContainer = styled.div`
  line-height: 1.15;
  text-size-adjust: 100%;
  font-size: 16px;
  color: rgb(48, 48, 48);
  font-family: " Sans", "Trebuchet MS", sans-serif;
  box-sizing: border-box;
  margin: 0px;
  border: 0px;
  margin-block: 0px;
  margin-inline: 0px;
  padding-block: 0px;
  padding-inline: 0px;
  width: 100%;
  max-width: 22.5rem;
  min-height: 7rem;
  padding: 2.5rem 0px 2.5rem 1.5rem;
`;

const HeaderDivContainerTittle = styled.div`
  line-height: 1.15;
  text-size-adjust: 100%;
  font-size: 16px;
  color: rgb(48, 48, 48);
  font-family: "LATAM Sans", "Trebuchet MS", sans-serif;
  box-sizing: border-box;
  margin: 0px;
  padding: 0px;
  border: 0px;
  margin-block: 0px;
  margin-inline: 0px;
  padding-block: 0px;
  padding-inline: 0px;
`;

const HeaderDivContainerLogo = styled.div`
  text-size-adjust: 100%;
  overflow: visible;
  cursor: pointer;
  margin: 0;
  display: inline-flex;
  outline: 0;
  position: relative;
  align-items: center;
  user-select: none;
  vertical-align: middle;
  justify-content: center;
  text-decoration: none;
  -webkit-appearance: none;
  -webkit-tap-highlight-color: transparent;
  min-width: 64px;
  box-sizing: border-box;
  line-height: 1.75;
  letter-spacing: 0.02857em;
  box-shadow: none;
  font-family: "LATAM Sans", "Trebuchet MS", sans-serif;
  font-weight: 700;
  border-radius: 0.5rem;
  text-transform: none;
  transition: none 0s ease 0s;
  background-color: transparent;
  border: 0.0625rem solid transparent;
  color: rgb(232, 17, 75);
  font-size: 1.125rem;
  height: 3.5rem;
  padding: 0px;
`;

const Container = styled.div`
  line-height: 1.15;
  text-size-adjust: 100%;
  font-size: 16px;
  color: rgb(48, 48, 48);
  font-family: "LATAM Sans", "Trebuchet MS", sans-serif;
  box-sizing: border-box;
  margin: 0px;
  border: 0px;
  margin-block: 0px;
  margin-inline: 0px;
  padding-block: 0px;
  padding-inline: 0px;
  display: flex;
  flex-direction: column;
  -webkit-box-pack: start;
  justify-content: flex-start;
  -webkit-box-flex: 1;
  flex-grow: 1;
  padding: 0px 1.5rem 2.5rem;
`;


function App() {
  const user = useSelector((state: any) => state.app.authUser as any) as any;
  const dispatch = useDispatch();
  //const history = useHistory();

  const fetchAuthUser = async () => {
    const response = await axios
      .get("http://localhost:5000/api/v1/auth/user", { withCredentials: true })
      .catch((err) => {
        console.log("Not properly authenticated");
        dispatch(setIsAuthenticated(false));
        dispatch(setAuthUser(null));
        //history.push("/login/error");
      });
  };
  // put on the url to call service SSO
  const redirectToGoogleSSO = async () => {
    let timer: NodeJS.Timeout | null = null;
    const googleLoginURL = "http://localhost:5000/api/v1/login/google";
    // eslint-disable-next-line
    const popUp = window.open(
      googleLoginURL,
      "_blank",
      "width=500, height=600"
    );

    if (popUp) {
      timer = setInterval(() => {
        if (popUp.closed) {
          console.log("Estas autentificado");
          if (timer) clearInterval(timer);
        }
      }, 500);
    }
  };

  return (
    <AppContainer>
      <Routes>
        <Route
          path="/"
          element={
            <LoginContainer>
              <HeaderContainer>
                <HeaderDivContainerTittle>
                  <h1> Bienvenidos </h1>
                </HeaderDivContainerTittle>
                <Link to="/login" rel="">
                  Iniciar sesión
                </Link>
              </HeaderContainer>
            </LoginContainer>
          }
        ></Route>
        <Route
          path="/login"
          element={
            <GoogleContainer>
              <GoogleButton onClick={redirectToGoogleSSO} />
            </GoogleContainer>
          }
        ></Route>

        <Route path="/login/success" element={<LoginSuccess />}></Route>

        <Route
          path="/login/error"
          element={
            <div>
              <h2>Error en iniciar sesión. Intente más tarde</h2>
            </div>
          }
        ></Route>
      </Routes>
    </AppContainer>
  );
}

export default App;
