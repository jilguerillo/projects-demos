import React, { useEffect } from "react";

export function LoginSuccess() {
  useEffect(() => {
    setTimeout(() => {
      window.close();
    }, 1200);
  }, []);

  return <div>Gracias por Iniciar Sesión!</div>;
}
 