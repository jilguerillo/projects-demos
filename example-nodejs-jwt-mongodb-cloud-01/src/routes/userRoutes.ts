import { Router } from "express";
import { getUsers } from "../controllers/userController";
import authMiddleware from "../middleware/authMiddleware";

const userRouter = Router();

userRouter.use(authMiddleware); // Protege las rutas con autenticación JWT

userRouter.get("/", getUsers);

export default userRouter;
