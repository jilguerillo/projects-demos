import { Router } from "express";
import { signup, login } from "../controllers/authController";

const authRouter = Router();

authRouter.route("/signup").post(signup);
authRouter.route("/login").post(login);

export default authRouter;
