import express, { Application } from "express";
import mongoose from "mongoose";
import config from "./config/config";
import authRouter from "./routes/authRoutes";
import userRouter from "./routes/userRoutes";

const app: Application = express();
const port = 3000;

app.use(express.json());

// Conexión a MongoDB en la nube
mongoose.connect(config.mongoURI);

// Define rutas
app.use("/auth", authRouter);
app.use("/users", userRouter);

// Rutas para /auth, /auth/login y /auth/signup
app.get("/auth", (req, res) => {
  // Lógica para la ruta /auth
  res.send("Página de autenticación");
});

app.get("/auth/login", (req, res) => {
  // Lógica para la ruta /auth/login
  res.send("Página de inicio de sesión");
});

app.get("/auth/signup", (req, res) => {
  // Lógica para la ruta /auth/signup
  res.send("Página de registro");
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
