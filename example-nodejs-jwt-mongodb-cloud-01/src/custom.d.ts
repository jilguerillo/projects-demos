/*
The custom.d.ts file is a TypeScript custom definition file that is used to extend 
or modify the TypeScript type definitions for your project. It allows you to add 
custom properties or types to existing interfaces or declare types specific to your project.

In the context of Express.js and other frameworks, custom.d.ts is used to extend 
the TypeScript type definitions for requests (Request) and responses (Response). 
By adding this custom definition file, you can declare additional properties that 
you want to use in requests or responses in your application without having to modify 
the TypeScript definitions of the original package directly.

The purpose of custom.d.ts is to provide a clean and organized way to extend TypeScript 
definitions to meet the specific needs of your project, without affecting compatibility 
with other packages and modules.
*/

import { Request } from "express";

declare module "express" {
    interface Request {
        user?: any;
    }
}
