import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class Solution {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numTestCases = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < numTestCases; i++) {
            String pattern = scanner.nextLine();

            try {
                Pattern.compile(pattern);
                System.out.println("Valid");
            } catch (Exception e) {
                System.out.println("Invalid");
            }
        }
    }
}

//Explicación:

//    Importación: Se importa la clase Pattern del paquete java.util.regex.
//   Entrada: Se lee el número de casos de prueba y luego se itera para cada caso.
//    Compilación: Se intenta compilar el patrón usando Pattern.compile(pattern).
//    Manejo de excepciones: Si la compilación es exitosa, se imprime "Valid". Si hay un error de sintaxis (PatternSyntaxException), se //imprime "Invalid".
//    Salida: Se muestra el resultado para cada caso de prueba.
//
//Este código aprovecha la propiedad de que un patrón de regex es válido solo si se puede compilar con Pattern.compile. Si la compilación es exitosa, la sintaxis es correcta. Si ocurre un error, la sintaxis es inválida.
