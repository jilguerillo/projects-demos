package com.register.tasks.entity;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "task")
public class TaskEntity {

    @Id
    private ObjectId id;
    
    @NotNull
    private String uuid; // Nuevo campo UUID autoincremental

    @NotBlank(message = "La descripción no puede estar en blanco")
    @Length(max = 255, message = "La descripción debe tener como máximo {max} caracteres")
    private String descripcion;

    @NotNull(message = "La fecha de creación no puede ser nula")
    private LocalDateTime fechaCreacion;

    @NotNull(message = "El campo vigente no puede ser nulo")
    private Boolean vigente;
}
