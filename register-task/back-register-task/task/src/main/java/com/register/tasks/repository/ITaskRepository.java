package com.register.tasks.repository;

import com.register.tasks.entity.TaskEntity;
import java.util.Optional;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.messaging.Task;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITaskRepository extends MongoRepository<TaskEntity, String>{ 

    public Optional<TaskEntity> findById(ObjectId objectId);

    public Optional<TaskEntity> findByUuid(String uuid);
}
