package com.register.tasks.controller;

import com.register.tasks.entity.TaskEntity;
import com.register.tasks.repository.ITaskRepository;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.bson.types.ObjectId;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    private final ITaskRepository taskRepository;

    public TaskController(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @GetMapping
    public List<TaskEntity> getAllTasks() {
        return taskRepository.findAll();
    }

    @PostMapping
    public TaskEntity addTask(@RequestBody TaskEntity task) {
        task.setId(new ObjectId());
        task.setUuid(UUID.randomUUID().toString()); // Generar y asignar el nuevo UUID
        return taskRepository.save(task);
    }

    @PutMapping("/{uuid}")
    public ResponseEntity<TaskEntity> updateTask(@PathVariable String uuid, @RequestBody TaskEntity taskDetails) {
        Optional<TaskEntity> task = taskRepository.findByUuid(uuid);
        if (task.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        task.get().setDescripcion(taskDetails.getDescripcion());
        task.get().setFechaCreacion(taskDetails.getFechaCreacion());
        task.get().setVigente(taskDetails.getVigente());
        TaskEntity updatedTask = taskRepository.save(task.get());
        return ResponseEntity.ok(updatedTask);
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<Void> deleteTask(@PathVariable String uuid) {
        Optional<TaskEntity> task = taskRepository.findByUuid(uuid);
        if (task.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        taskRepository.delete(task.get());
        return ResponseEntity.noContent().build();
    }
}
