module.exports = {
    transform: {
      '^.+\\.js$': 'babel-jest',
    },
    moduleNameMapper: {
      '^axios$': '<rootDir>/node_modules/axios',
    },
  };