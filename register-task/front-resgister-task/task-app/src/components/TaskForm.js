import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTask, fetchTasks } from '../actions/taskActions';

const TaskForm = () => {
  const dispatch = useDispatch();
  const [descripcion, setDescripcion] = useState('');
  const [vigente, setVigente] = useState(true); // Valor inicial: vigente
  const fechaCreacion = new Date(); // Fecha actual del sistema
  const [error, setError] = useState('');

  const handleDescripcionChange = e => {
    setDescripcion(e.target.value);
  };

  const handleVigenteChange = e => {
    setVigente(e.target.checked);
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (descripcion.trim() === '') {
      setError('El campo Descripción es requerido');
      return;
    }
    setError('');
    dispatch(addTask({ descripcion, vigente, fechaCreacion }));
    setDescripcion('');
    setVigente(true); // Restablecer a valor inicial
  };

  return (
    <div>
      <h2>Agregar tarea</h2>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={descripcion}
          onChange={handleDescripcionChange}
          placeholder="Descripción"
          required
        />
        <label>
          <input
            type="checkbox"
            checked={vigente}
            onChange={handleVigenteChange}
            required
          />
          Vigente
        </label>
        <button type="submit">Agregar</button>
        {error && <p>{error}</p>}
      </form>
    </div>
  );
};

export default TaskForm;
