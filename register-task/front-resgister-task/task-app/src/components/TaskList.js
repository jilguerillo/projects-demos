import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import TaskItem from './TaskItem';

import { useEffect } from 'react';
import { fetchTasks } from '../actions/taskActions';

const TaskList = () => {
  const dispatch = useDispatch();
  const tasks = useSelector(state => state.tasks);
  console.log("---> get all", tasks)

  useEffect(() => {
    dispatch(fetchTasks());
  }, [dispatch]);

  return (
    <div>
      <h2>Lista de tareas</h2>
      {tasks.map(task => (
        <TaskItem key={task.uuid} task={task} />
      ))}
    </div>
  );
};

export default TaskList;
