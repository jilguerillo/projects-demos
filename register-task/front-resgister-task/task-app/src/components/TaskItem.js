import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { updateTask, removeTask } from '../actions/taskActions';

const TaskItem = ({ task }) => {
  const dispatch = useDispatch();
  const [isModalOpen, setModalOpen] = useState(false);
  const [updatedDescription, setUpdatedDescription] = useState('');
  const [updatedVigente, setUpdatedVigente] = useState(task.vigente);

  const handleEdit = () => {
    setModalOpen(true);
    setUpdatedDescription(task.descripcion);
    setUpdatedVigente(task.vigente);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const handleSave = () => {
    const updatedTask = { ...task, descripcion: updatedDescription, vigente: updatedVigente };
    dispatch(updateTask(task.uuid, updatedTask));
    setModalOpen(false);
  };

  const handleRemove = () => {
    dispatch(removeTask(task.uuid));
  };

  const formatDate = date => {
    const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
    return new Date(date).toLocaleDateString('es-ES', options);
  };

  return (
    <div>
      <div>{task.descripcion}</div>
      <div>{formatDate(task.fechaCreacion)}</div>
      <div>{task.vigente ? 'Vigente' : 'No vigente'}</div>
      <button onClick={handleEdit}>Editar</button>
      <button onClick={handleRemove}>Eliminar</button>

      {isModalOpen && (
        <div className="modal">
          <input
            type="text"
            value={updatedDescription}
            onChange={e => setUpdatedDescription(e.target.value)}
          />
          <label>
            <input
              type="checkbox"
              checked={updatedVigente}
              onChange={e => setUpdatedVigente(e.target.checked)}
            />
            Vigente
          </label>
          <button onClick={handleSave}>Guardar</button>
          <button onClick={handleModalClose}>Cerrar</button>
        </div>
      )}
    </div>
  );
};

export default TaskItem;
