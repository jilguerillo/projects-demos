import axios from 'axios';

    it("test_fetch_tasks_successfully", async () => {
        const dispatch = jest.fn();
        axios.get.mockResolvedValue({ data: [{ id: 1, task: "Task 1" }] });
        await fetchTasks()(dispatch);
        expect(dispatch).toHaveBeenCalledWith({
            type: 'FETCH_TASKS',
            payload: [{ id: 1, task: "Task 1" }],
        });
    });