import axios from 'axios';

const url = 'http://localhost:9091';

export const fetchTasks = () => {
  return dispatch => {
    axios.get(url + '/api/tasks')
      .then(response => {
        dispatch({
          type: 'FETCH_TASKS',
          payload: response.data,
        });
      })
      .catch(error => {
        // Manejo de errores
      });
  };
};

export const addTask = task => {
  console.log("---> ADDING " + task);
  return dispatch => {
    axios.post(url + '/api/tasks', task)
      .then(response => {
        dispatch({
          type: 'ADD_TASK',
          payload: response.data,
        });
      })
      .catch(error => {
        // Manejo de errores
      });
  };
};

export const updateTask = (id, task) => {
  return dispatch => {
    axios.put(url + `/api/tasks/${id}`, task)
      .then(response => {
        dispatch({
          type: 'UPDATE_TASK',
          payload: response.data,
        });
        dispatch(fetchTasks()); // Agregar la llamada para obtener las tareas actualizadas
      })
      .catch(error => {
        // Manejo de errores
      });
  };
};


export const removeTask = (id) => {
  return dispatch => {
    axios.delete(url + `/api/tasks/${id}`)
      .then(() => {
        dispatch({
          type: 'DELETE_TASK',
          payload: id,
        });
        dispatch(fetchTasks()); // Agregar la llamada para obtener las tareas actualizadas
      })
      .catch(error => {
        // Manejo de errores
      });
  };
};


