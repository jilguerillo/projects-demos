# Project Demo API REST 1

#### Este proyecto es una API REST construida con Spring Boot 3.0 y Java 21. Maneja la creación y consulta de usuarios con validación básica de email y contraseña.

## Requisitos

- **Java 21**
- **Gradle**
- **H2 (PostgreSQL)**

## Clonar el proyecto

Como este proyecto está dentro de un repositorio más grande, puedes clonar solo este subdirectorio siguiendo estos pasos:

1. Clona el repositorio principal en modo "sparse":

```
git clone --filter=blob:none --sparse https://gitlab.com/jilguerillo/projects-demos.git
```

2. Cambia al directorio del repositorio:

```
cd projects-demos
```
3. Inicializa el sparse-checkout:

```
git sparse-checkout init --cone
```
4. Descarga solo el subdirectorio project.demo.apirest1:

``` 
git sparse-checkout set project.demo.apirest1
```

5. Cambia al directorio del proyecto:

```
cd  project.demo.apirest1/
```

## Configuración

### Para la base de datos h2 en memoria 

Una vez levantada la aplicación en local ir a : http://localhost:8080/apirest1/h2-console 

### Las credenciales son :

- username=sa
- password=password
- datasource.url=jdbc:h2:mem:testdb

----------------------------------------------------

### En caso de usar la base de datos en supabase, descomentar lo que corresponde a su configuración en los properties y seguir con lo siguiente : 

### Establece las siguientes variables de entorno antes de ejecutar la aplicación:

```
export PASSWORD_DB=<PASSWORD> \
export USER_DB=postgres \
export USER_SECURITY=user \
export PASS_SECURITY=<PASSWORD>
```

## Base de Datos
### El proyecto usa PostgreSQL. Asegúrate de que la base de datos esté configurada correctamente. La conexión está definida en el archivo application.properties:

###### properties

```
spring.datasource.url=jdbc:postgresql://aws-0-us-east-1.pooler.supabase.com:6543/postgres
spring.datasource.username=${USER_DB}
spring.datasource.password=${PASSWORD_DB}
```

##### En caso de no poder acceder a la base de datos actual a causa de falta de credenciales o por problemas técnicos 
##### por parte del cliente se deja la query de creación de las tables

```
CREATE TABLE users (
id UUID PRIMARY KEY,                -- ID de usuario como UUID
name VARCHAR(255) NOT NULL,          -- Nombre del usuario
email VARCHAR(255) NOT NULL,         -- Email del usuario
created TIMESTAMP,                   -- Fecha de creación
modified TIMESTAMP,                  -- Fecha de modificación
last_login TIMESTAMP,                -- Última fecha de login
token UUID,                          -- Token como UUID
isactive BOOLEAN DEFAULT true        -- Estado de actividad del usuario (por defecto activo)
);

-- Table: phones
CREATE TABLE phones (
id UUID PRIMARY KEY,                 -- ID del teléfono como UUID
number VARCHAR(255) NOT NULL,        -- Número de teléfono
user_id UUID,                        -- ID del usuario al que pertenece este teléfono
CONSTRAINT fk_user
FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE -- Relación con la tabla users
);
```

----------------------------------------------------

## Ejecutar la Aplicación
### Usa los siguientes comandos para ejecutar la aplicación:

```
./gradlew bootRun
```

## La API estará disponible en:

http://localhost:8080/apirest1

### Endpoints Principales

#### Crear un Usuario
#### POST /api/users
##### Ejemplo de petición:

```
{
"name": "Juan Rodriguez",
"email": "juaka@domain.com",
"password": "hu2AS12345678",
"phones": [
{
"number": "1234567",
"citycode": "1",
"countrycode": "57"
}
]
}
```

## Documentación de la API
### Puedes acceder a la [documentación de la API](https://gitlab.com/jilguerillo/projects-demos/-/blob/main/project.demo.apirest1/deploy/endpoints/openapi.yaml?ref_type=heads) (Swagger) en:

```
http://localhost:8080/apirest1/swagger-ui-custom.html
```



## Actuator
### Para verificar el estado de la aplicación:

Health: http://localhost:8080/apirest1/actuator/health

Info: http://localhost:8080/apirest1/actuator/info

### Resumen:

- Este `README.md` explica cómo clonar solo el subproyecto `project.demo.apirest1` usando `sparse-checkout` en GitLab.
- Además, proporciona instrucciones claras para configurar y ejecutar la aplicación.