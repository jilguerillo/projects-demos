package com.project.demo.api.domain.service;

import com.project.demo.api.domain.entity.User;
import com.project.demo.api.domain.service.UserService;
import com.project.demo.api.dto.UserDto;
import com.project.demo.api.dto.UserResponse;
import com.project.demo.api.repository.UserRepository;
import com.project.demo.api.repository.PhoneRepository;
import com.project.demo.api.repository.mapper.UserMapper;
import com.project.demo.api.repository.mapper.PhoneMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PhoneRepository phoneRepository;

    @Mock
    private UserMapper userMapper;

    @Mock
    private PhoneMapper phoneMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(userService, "emailRegex", "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$");
        ReflectionTestUtils.setField(userService, "passwordRegex", "^(?=.*[A-Z])(?=.*\\d).{8,}$");
    }

    @Test
    void createUserSuccess() {
        UserDto userDto = new UserDto();
        userDto.setName("Juan Rodriguez");
        userDto.setEmail("juan@rodriguez.org");
        userDto.setPassword("Hunter2345678");

        User user = new User();
        user.setId(UUID.randomUUID());
        user.setCreated(new Date());
        user.setModified(new Date());
        user.setLast_login(new Date());
        user.setToken(UUID.randomUUID());
        user.setIsactive(true);

        when(userMapper.userDtoToUser(any(UserDto.class))).thenReturn(user);
        when(userRepository.save(any(User.class))).thenReturn(user);

        UserResponse result = userService.createUser(userDto);

        assertNotNull(result, "UserResponse should not be null");
        assertEquals(user.getId(), result.getId(), "User ID should match");
        assertEquals(user.getCreated(), result.getCreated(), "Created date should match");
        assertEquals(user.getModified(), result.getModified(), "Modified date should match");
        assertEquals(user.getLast_login(), result.getLast_login(), "Last login date should match");
    }
    @Test
    void createUserEmailAlreadyExists() {
        UserDto userDto = new UserDto();
        userDto.setName("Juan Rodriguez");
        userDto.setEmail("juan@rodriguez.org");
        userDto.setPassword("Hunter2345678");

        User user = new User();
        user.setId(UUID.randomUUID());
        user.setCreated(new Date());
        user.setModified(new Date());
        user.setLast_login(new Date());
        user.setToken(UUID.randomUUID());
        user.setIsactive(true);

        when(userRepository.existsByEmail(anyString())).thenReturn(true);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            userService.createUser(userDto);
        });

        assertEquals("El correo ya registrado.", exception.getMessage(), "Error message should match");
    }

    @Test
    void createUserInvalidPassword() {
        UserDto userDto = new UserDto();
        userDto.setName("Juan Rodriguez");
        userDto.setEmail("juan@rodriguez.org");
        userDto.setPassword("short");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            userService.createUser(userDto);
        });

        assertEquals("El formato de la clave es inválido.", exception.getMessage());
    }

    @Test
    void getUserByIdSuccess() {

        UUID userId = UUID.randomUUID();
        User user = new User();
        user.setId(userId);
        user.setName("Juan Rodriguez");

        UserDto userDto = new UserDto();
        userDto.setId(userId);
        userDto.setName("Juan Rodriguez");

        when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));
        when(userMapper.userToUserDto(any(User.class))).thenReturn(userDto);

        UserDto result = userService.getUserById(userId);

        assertNotNull(result);
        assertEquals(userId, result.getId());
        assertEquals("Juan Rodriguez", result.getName());
    }

    @Test
    void getUserByIdNotFound() {
        UUID userId = UUID.randomUUID();

        when(userRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        UserDto result = userService.getUserById(userId);

        assertNull(result);
    }
}
