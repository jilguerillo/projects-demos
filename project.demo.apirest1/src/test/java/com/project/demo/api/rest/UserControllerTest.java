package com.project.demo.api.rest;

import com.project.demo.api.domain.service.UserService;
import com.project.demo.api.dto.UserDto;
import com.project.demo.api.dto.UserResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createUserSuccess() {
        UserDto userDto = new UserDto();
        userDto.setName("Juan Rodriguez");
        userDto.setEmail("juan@rodriguez.org");
        userDto.setPassword("Hunter2");

        UserResponse userResponse = UserResponse.builder()
                .id(UUID.randomUUID())
                .created(new Date())
                .modified(new Date())
                .last_login(new Date())
                .token(UUID.randomUUID())
                .isactive(true)
                .build();

        when(userService.createUser(any(UserDto.class))).thenReturn(userResponse);

        ResponseEntity<?> response = userController.createUser(userDto);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertInstanceOf(UserResponse.class, response.getBody());

        UserResponse result = (UserResponse) response.getBody();
        assertEquals(userResponse.getId(), result.getId());
    }

    @Test
    void createUserEmailAlreadyExists() {
        UserDto userDto = new UserDto();
        userDto.setName("Juan Rodriguez");
        userDto.setEmail("juan@rodriguez.org");

        doThrow(new IllegalArgumentException("El correo ya registrado."))
                .when(userService).createUser(any(UserDto.class));

        ResponseEntity<?> response = userController.createUser(userDto);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("El correo ya registrado.", response.getBody());
    }

    @Test
    void createUserInvalidPassword() {
        UserDto userDto = new UserDto();
        userDto.setName("Juan Rodriguez");
        userDto.setEmail("juan@rodriguez.org");
        userDto.setPassword("short");

        doThrow(new IllegalArgumentException("El formato de la clave es inválido."))
                .when(userService).createUser(any(UserDto.class));

        ResponseEntity<?> response = userController.createUser(userDto);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("El formato de la clave es inválido.", response.getBody());
    }

    @Test
    void getUserByIdSuccess() {
        UUID userId = UUID.randomUUID();
        UserDto userDto = new UserDto();
        userDto.setId(userId);
        userDto.setName("Juan Rodriguez");

        when(userService.getUserById(any(UUID.class))).thenReturn(userDto);

        ResponseEntity<UserDto> response = userController.getUserById(userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(userId, response.getBody().getId());
    }
}