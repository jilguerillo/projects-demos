INSERT INTO users (id, name, password, email, created, modified, last_login, token, isactive)
VALUES (UUID(), 'Juan Rodriguez', 'Amin12345678', 'juan@domain.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, UUID(), true);

INSERT INTO phones (id, number, user_id)
VALUES (UUID(), '1234567', (SELECT id FROM users WHERE email = 'juan@domain.com'));
