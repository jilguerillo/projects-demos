package com.project.demo.api.domain.service;

import com.project.demo.api.domain.entity.Phone;
import com.project.demo.api.domain.entity.User;
import com.project.demo.api.dto.PhoneDto;
import com.project.demo.api.dto.UserDto;
import com.project.demo.api.dto.UserResponse;
import com.project.demo.api.repository.PhoneRepository;
import com.project.demo.api.repository.UserRepository;
import com.project.demo.api.repository.mapper.PhoneMapper;
import com.project.demo.api.repository.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PhoneMapper phoneMapper;

    @Value("${regex.email}")
    private String emailRegex;

    @Value("${regex.password}")
    private String passwordRegex;

    public UserResponse createUser(UserDto userDto) {

        if (!userDto.getEmail().matches(emailRegex)) {
            throw new IllegalArgumentException("El formato del correo es inválido.");
        }

        if (!userDto.getPassword().matches(passwordRegex)) {
            throw new IllegalArgumentException("El formato de la clave es inválido.");
        }

        if (userRepository.existsByEmail(userDto.getEmail())) {
            throw new IllegalArgumentException("El correo ya registrado.");
        }

        try {
            User user = userMapper.userDtoToUser(userDto);
            user.setLast_login(new Date());
            User savedUser = userRepository.save(user);

            if (userDto.getPhones() != null) {
                for (PhoneDto phoneDto : userDto.getPhones()) {
                    Phone phone = phoneMapper.phoneDtoToPhone(phoneDto);
                    phone.setUser(savedUser);
                    phoneRepository.save(phone);
                }
            }

            return UserResponse.builder()
                    .id(savedUser.getId())
                    .created(savedUser.getCreated())
                    .modified(savedUser.getModified())
                    .last_login(savedUser.getLast_login())
                    .token(savedUser.getToken())
                    .isactive(savedUser.isIsactive())
                    .build();

        } catch (RuntimeException e) {
            log.error("Error al crear el usuario", e);
            throw new RuntimeException("Error al procesar la solicitud");
        }
    }

    public UserDto getUserById(UUID id) {
        User user = userRepository.findById(id).orElse(null);
        return userMapper.userToUserDto(user);
    }
}
