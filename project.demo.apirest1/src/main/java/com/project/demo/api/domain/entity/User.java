package com.project.demo.api.domain.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    private UUID id;
    private String name;
    private String password;
    private String email;
    private Date created;
    private Date modified;
    private Date last_login;
    private UUID token;
    private boolean isactive;
    @OneToMany(mappedBy = "user")
    private List<Phone> phones = new ArrayList<>();

    @PrePersist
    public void prePersist() {

        Date currentDate = new Date();
        this.created = currentDate;
        this.modified = currentDate;
        this.last_login = currentDate;
        this.isactive = true;

        if (id == null) {
            id = UUID.randomUUID();
        }
        if (token == null) {
            token = UUID.randomUUID();
        }
    }

    @PreUpdate
    protected void onUpdate() {
        this.modified = new Date();
    }
}
