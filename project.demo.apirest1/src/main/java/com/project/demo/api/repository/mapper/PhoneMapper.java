package com.project.demo.api.repository.mapper;

import com.project.demo.api.domain.entity.Phone;
import com.project.demo.api.dto.PhoneDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PhoneMapper {

    PhoneDto phoneToPhoneDto(Phone phone);

    @Mapping(target = "user", ignore = true)
    Phone phoneDtoToPhone(PhoneDto phoneDto);
}
