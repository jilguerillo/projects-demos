package com.project.demo.api.repository.mapper;

import com.project.demo.api.domain.entity.User;
import com.project.demo.api.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {PhoneMapper.class})
public interface UserMapper {

    UserDto userToUserDto(User user);

    @Mapping(target = "phones", source = "phones")
    User userDtoToUser(UserDto userDto);
}