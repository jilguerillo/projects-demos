package com.project.demo.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private UUID id;
    private String name;
    private String password;
    private String email;
    private Date created;
    private Date modified;
    private Date last_login;
    private UUID token;
    private boolean isactive;
    private List<PhoneDto> phones;
}