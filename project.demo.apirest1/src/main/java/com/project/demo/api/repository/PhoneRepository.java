package com.project.demo.api.repository;

import com.project.demo.api.domain.entity.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PhoneRepository extends JpaRepository<Phone, UUID> {
    // List<Phone> findAll();

    // Optional<Phone> findById(UUID id);
}